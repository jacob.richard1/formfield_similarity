FROM python:3.10
ADD distance.py /
RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir nltk
CMD ls home/
EXPOSE 8000
