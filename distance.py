import csv
import sys
import nltk
import string
import asyncio



first_letter = 'a' if len(sys.argv) == 1 else sys.argv[1].lower()
print(first_letter)
file = open("./input/formfield_guid_and_label_and_type.csv", "r")
data = [ [r[0],r[1].lower(),r[1], r[2]] for r in list(csv.reader(file, delimiter=",")) if r[1].lower().startswith(first_letter)]
file.close()
print(len(data))
# sort by first letter of sentence, ignoring capitalization
data.sort(key=lambda x: x[1])
print("sorted")

def first_word_match(sent, other):
    return sent[1].split(' ')[0] == other[1].split(' ')[0]

async def edit_distance(sentence):
    distance_= nltk.edit_distance(sentence[0][3], sentence[1][3]) # check edit distance ignoring punctuation
    if distance_ < 3:
        return [sentence[0][0],sentence[0][1],sentence[0][2], sentence[1][0],sentence[1][1],sentence[1][2]]
    return None
async def do_multiple_tasks(output):
    return await asyncio.gather(*[edit_distance(sentence) for sentence in output])
def write_output(first_letter, output, index):
    # https://python.gotrained.com/nltk-edit-distance-jaccard-distance/#Edit_Distance
    output = asyncio.run(do_multiple_tasks(output))
    output = [appender for appender in output if appender is not None]
    output.sort(key=lambda x: x[0][1], reverse=True)  # sort on the label
    with open(f'./output/{first_letter}-{index}-out.csv', 'w') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(['guid1', 'label1', 'type1', 'guid2', 'label2', 'type2'])
        csvwriter.writerows(output)

translator = str.maketrans('', '', string.punctuation)
output = []
counter = 0
write_chunk_length = 500000
# put sentences where first word of sentence matches in a list to parse
for i, sent in enumerate(data):
    next_ = next((j for j, other in enumerate(data) if first_word_match(sent, other) and j > i), None)
    if next_ is None:
        continue
    sent_no_punct = sent[1].translate(translator) # ignore punctuation
    for j, other in enumerate(data[next_:]):
        if not first_word_match(sent, other): # the first word of the labels should match
            break
        other_no_punct = other[1].translate(translator)
        len_diff = len(sent_no_punct) - len(other_no_punct)
        if abs(len_diff) > 2: # if the difference is more than 2, then the edit distance is more than 2
            continue
        output.append([[other[0], other[2], other[3], sent_no_punct],[sent[0], sent[2], sent[3], other_no_punct]]) # append the guid and label and type
    if i % 1000 == 0:
        print(f'{i}: {len(output)}')
    if len(output) > write_chunk_length:
        print("writing out")
        write_output(first_letter, output, counter)
        counter = counter + 1
        output = []
write_output(first_letter, output, counter)