## Input
get input data like

- run below query in looker prod_read_replica. save the query to a csv and name it ./input/guid_and_label.csv. columns of csv are guid,label,type.
- select guid, label, type from public.form_builder_formfield 
where usage_count in (1,2,3) and not is_repeatable and 
id not in (select form_field_id from public.entities_entityformfield ) 
and label is not null and label <> ''
order by 2 desc
--------------------------------------------------------------------------

## Run Program
- specify the letter you want to run. find matching formfields where the label starts with that letter.
- bash ./main.sh {letter}
- for example:
- bash ./main.sh a
- bash ./main.sh b
- to run all letters at once, dont provide a value. run like
- bash ./main.sh
--------------------------------------------------------------------------

## Output
- get output data like
- ./output/{first_letter}{batch}-out.csv
--------------------------------------------------------------------------

## Requirements
The only requirement is Docker. If seeing out of memory issues, increase the memory in docker desktop.
- For Linux
  - Docker 17.06+ - [Installation Guide](https://docs.docker.com/engine/install/ubuntu/)  **Note: DO NOT install from snap, that version is known to not work properly**
    - Please also see https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user
  - docker-compose - [Installation Guide](https://dockerlabs.collabnix.com/intermediate/workshop/DockerCompose/How_to_Install_Docker_Compose.html)
- For Mac
  - Download Docker Desktop from [Download link](https://docs.docker.com/desktop/mac/install/) which includes docker-compose - 
    - Once installed, update memory to 8 GB under preferences > resourcesnstalled, update memory to 8 GB under preferences > resources