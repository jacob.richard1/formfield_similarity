import sys
import string
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue import DynamicFrame
import boto3
import json
from datetime import date


## @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args) # --first_letter=a, --first_letter=b
ssm_client = boto3.client('ssm')
param_name="first_letter_glue_formfield_similarity"
first_letter=ssm_client.get_parameter(Name=param_name)['Parameter']['Value'] #first letter of data to run queries on. like a b c, ect
# abcd...zabcd
next_letter = 'a' if (first_letter >= 'z') else chr(ord(first_letter) + 1)
ssm_client.put_parameter(Name=param_name, Value=next_letter, Overwrite=True)
logger = glueContext.get_logger()

glue_client = boto3.session.Session().client('glue')
s3_resource = boto3.resource('s3')
s3_bucket = s3_resource.Bucket('formfield-similarity')
# delete from tmp
s3_bucket.objects.filter(Prefix=f'tmp/{first_letter}').delete()
# delete from /output/letter
s3_bucket.objects.filter(Prefix=f'output/{first_letter}').delete()
s3 = boto3.client('s3')
def truncate_letter(first_letter):
    preact=f"delete from public.formfield_similarity where first_letter = '{first_letter}'; delete from public.formfield_similarity where first_letter = 'notaguid' ;"
    post=f"delete from public.formfield_similarity where first_letter = 'notaguid' ;"
    dynamicFrame = glueContext.create_dynamic_frame.from_options(
        connection_type="s3",
        connection_options={"paths": [f"s3://formfield-similarity/table_for_truncate.csv"]},
        format="csv",
        format_options={
            "withHeader": True,
        },
    )
    datasink2 = glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dynamicFrame,
        catalog_connection=f"analytics",
        connection_options={"dbtable": "formfield_similarity", "database": "indio", "bulkSize": "3", "preactions": preact, 'postactions': post},
        redshift_tmp_dir=args[f"TempDir"],
        transformation_ctx=f"datasink2")

truncate_letter(first_letter)

def sparkSqlQuery(glueContext, query, mapping, transformation_ctx) -> DynamicFrame:
    for alias, frame in mapping.items():
        frame.toDF().createOrReplaceTempView(alias)
    result = spark.sql(query)
    return [list(data[0:4]) for data in result.select('*').collect()]

# Script generated for node formfield
formfield_node = glueContext.create_dynamic_frame.from_catalog(
    database="production",
    table_name="indio_public_form_builder_formfield",
    transformation_ctx="formfield_node",
)

# Script generated for node entityformfield
entityformfield_node = glueContext.create_dynamic_frame.from_catalog(
    database="production",
    table_name="indio_public_entities_entityformfield",
    transformation_ctx="entityformfield",
)

# filter on labels where the first letter matches the input
SqlQuery0 = f"""
select guid, label, 
CASE
WHEN type in ('text', 'text-area', 'range', 'currency', 'numeric', 'number') THEN 'text-group'
WHEN type in ('radio', 'multi-choice', 'dropdown') THEN 'button-group'
WHEN type in ('boolean') THEN 'boolean-group'
ELSE type
END AS type_with_groups, get_json_object(attributes, '$.options') as options_for_button_group
from form_builder_formfield
where usage_count in (1,2,3) and not is_repeatable and id not in 
(select form_field_id from entities_entityformfield ) and label is not null and label <> '' 
and lower(label) like '{first_letter}%'
and type <> 'file' and type <> 'date' 
order by 2 desc
"""
SQLQuery_node = sparkSqlQuery(
    glueContext,
    query=SqlQuery0,
    mapping={
        "form_builder_formfield": formfield_node,
        "entities_entityformfield": entityformfield_node,
    },
    transformation_ctx="SQLQuery_node",
)
s3.put_object(
    Body=bytes(f"Date Of Last Run For Letter {first_letter}: {date.today().strftime('%B-%d-%Y')}", 'utf-8'),
    Bucket='formfield-similarity',
    Key=f"output/{first_letter}/_Date_Of_Last_Run_Was_{date.today().strftime('%B-%d-%Y')}.txt"
)
data = [ [r[0],r[1].lower(),r[1], r[2], r[3]] for r in SQLQuery_node]
# sort by first letter of sentence, ignoring capitalization
data.sort(key=lambda x: x[1])
def first_word_match(sent, other):
    return sent[1].split(' ')[0] == other[1].split(' ')[0]
def field_types_can_merge(sent, other):
    # field types have to match up for a merge
    # groups should match in this case
    if sent[3] in ('text-group', 'button-group', 'boolean-group') and sent[3] != other[3]:
        return False
    # attributes.options should match in this case. like buttons should have the same dropdown options
    if sent[3] in ('button-group') and sent[4] != other[4]:
        return False
    return True
def write_output(first_letter, output, counter):
    filename_tmp = f'tmp/{first_letter}/{counter}.json'
    filename_out = f'output/{first_letter}/{counter}.csv'
    s3.put_object(
        Body=json.dumps(output),
        Bucket='formfield-similarity',
        Key=filename_tmp
    )
    # call glue batch in parallel
    glue_client.start_job_run(JobName='formfield_similarity_process_batch_to_redshift',
                              Arguments={'--filename_out': filename_out,
                                         '--filename_tmp': filename_tmp})

translator = str.maketrans('', '', string.punctuation)
output = []
counter = 0
write_chunk_length = 500000
rowid = 0
# put sentences where first word of sentence matches in a list to parse
for i, sent in enumerate(data):
    next_ = next((j for j, other in enumerate(data) if first_word_match(sent, other) and j > i), None)
    if next_ is None:
        continue
    sent_no_punct = sent[1].translate(translator) # ignore punctuation. used for computing edit distance in lambda
    for j, other in enumerate(data[next_:]):
        if not first_word_match(sent, other): # the first word of the labels should match
            break
        other_no_punct = other[1].translate(translator)
        len_diff = len(sent_no_punct) - len(other_no_punct)
        if abs(len_diff) > 2: # if the difference is more than 2, then the edit distance is more than 2
            continue
        if not field_types_can_merge(sent, other):
            continue
        output.append([[other[0], other[2], other[3], sent_no_punct],[sent[0], sent[2], sent[3], other_no_punct], rowid, first_letter]) # append the guid and label and type and sentence with no punctuation
        rowid = rowid + 1
    if i % 1000 == 0:
        logger.info(f'index {i}: {len(output)}')
    if len(output) > write_chunk_length:
        logger.info("writing out")
        write_output(first_letter, output, counter)
        counter = counter + 1
        output = []
    if rowid > 40000000:
        raise Exception("only less than 40,000,000 rows")
write_output(first_letter, output, counter)
job.commit()