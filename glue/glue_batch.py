import json
import boto3
import sys
import asyncio
from awsglue.utils import getResolvedOptions
import editdistance
import os
from datetime import date
from awsglue.transforms import *
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
# call this from aws glue 'formfield_similarity' job
async def edit_distance(sentence):
    # same as below but faster
    # distance_ = nltk.edit_distance(sentence[0][3], sentence[1][3])
    distance_ = editdistance.eval(sentence[0][3], sentence[1][3])  # check edit distance ignoring punctuation
    if distance_ < 3:
        return [sentence[0][0], sentence[0][1], sentence[0][2], \
                sentence[1][0], sentence[1][1], sentence[1][2], \
                sentence[2], sentence[3], date.today()
                ]
    return None

async def do_multiple_tasks(output):
    return await asyncio.gather(*[edit_distance(sentence) for sentence in output])

def write_output(output, filename, s3, glueContext):
    # https://python.gotrained.com/nltk-edit-distance-jaccard-distance/#Edit_Distance
    output = asyncio.run(do_multiple_tasks(output))
    output = [appender for appender in output if appender is not None]
    output.sort(key=lambda x: x[0][1], reverse=True)  # sort on the label
    output = [['formfield1_guid', 'formfield1_label', 'formfield1_type', \
              'formfield2_guid', 'formfield2_label', 'formfield2_type', \
              'row_id_of_letter', 'first_letter', 'date_of_last_update']] + output
    # write to csv
    output = '\n'.join(['"'+'","'.join(map(str, o))+'"' for o in output])

    s3.put_object(
        Body=bytes(output, 'utf-8'),
        Bucket='formfield-similarity',
        Key=filename
    )
    dynamicFrame = glueContext.create_dynamic_frame.from_options(
        connection_type="s3",
        connection_options={"paths": [f"s3://formfield-similarity/{filename}"]},
        format="csv",
        format_options={
            "withHeader": True,
        },
    )
    datasink2 = glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dynamicFrame,
        catalog_connection=f"analytics",
        connection_options={"dbtable": "formfield_similarity", "database": "indio", "bulkSize": "3"},
        redshift_tmp_dir=args[f"TempDir"],
        transformation_ctx=f"datasink2")
    # tar the file
    #os.system(f"aws s3 cp s3://formfield-similarity/{filename} - | gzip | aws s3 cp - s3://formfield-similarity/{filename}.gz")

## @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME', 'filename_tmp','filename_out'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
s3 = boto3.client('s3')
data = s3.get_object(
    Bucket='formfield-similarity',
    Key=args['filename_tmp'])['Body'].read()
write_output(json.loads(data), args['filename_out'], s3, glueContext)
s3.delete_object(
    Bucket='formfield-similarity',
    Key=args['filename_tmp'])
s3.delete_object(
    Bucket='formfield-similarity',
    Key=args['filename_out'])
job.commit()